# SugarScape in NetLogo (Modélisation multi-agents — TP)

| Author: **Sébastien Picault, INRAE** | Dec. 6, 2023                                                                                                   |
|---|----------------------------------------------------------------------------------------------------------------|
| License: [CC-BY-NC-SA](https://en.wikipedia.org/wiki/Creative_Commons_license) | ![CC-BY-NC-SA](https://upload.wikimedia.org/wikipedia/commons/1/12/Cc-by-nc-sa_icon.svg "License CC-BY-NC-SA") |

<span>
<table style="border: none;">
    <tr style="border: none;">
        <td style="padding: 25px; border: none;"><img alt="bloc marque Etat" src="img/Republique_Francaise.jpg" width="300px"></td>
        <td style="padding: 25px; border: none;"><img alt="logo INRAE" src="img/Logo-INRAE.jpg" width="400px"></td>
        <td style="padding: 25px; border: none;"><img alt="logo Oniris" src="img/Logo-Oniris.png" width="350px"></td>
        <td style="padding: 25px; border: none;"><img alt="logo Pays de la Loire" src="img/institut-agro.png" width="500px"></td>
    </tr>
</table>
</span>

Ces exercices s'inscrivent dans la spécialisation M2 "Eau, Energie, Climat, Atténuation et adaptation au changement climatique" (Institut Agro Rennes)

# À propos de NetLogo
![Language](https://img.shields.io/badge/language-NetLogo--6.4-8cd0c3.svg)

- Télécharger [NetLogo](https://ccl.northwestern.edu/netlogo/download.shtml)
- [Manuel utilisateur](https://ccl.northwestern.edu/netlogo/docs/)
- [Dictionnaire](https://ccl.northwestern.edu/netlogo/docs/dictionary.html) des commandes

# Présentation
Le modèle « SugarScape » a été conçu pour comprendre les effets de la complexification progressive de comportements au sein d’une population d’agents[^1]. 
En voici la déclinaison pour la plateforme de simulation NetLogo[^2]. 

L'environnement contient des ressources (par exemple de la nourriture). 
Des agents s'y déplacent avec pour simple objectif de survivre. 
Initialement un ensemble d'agents et deux tas de nourriture sont créés. 
Au fur et à mesure de l'expérience les agents se déplacent, se nourrissent, pendant que la nourriture « repousse » (se régénère). 
Si un agent n’a plus de réserve de nourriture, il meurt. 
Un graphique présente des informations sur les agents (nombre, niveau de nourriture moyen, etc.) tandis qu'un autre affiche des informations sur l'environnement (nourriture disponible, etc.). 
La simulation s'arrête s'il ne reste plus d'agent vivant.

## Lecture du code
Ouvrez le code fourni ([sugarscape0.nlogo](sugarscape0.nlogo)) et choisissez l’onglet « code ». Lisez attentivement les procédures et les fonctions.

- `init-patches` affecte la variable `food` aux patchs en deux tas circulaires avec un gradient (le centre est mieux doté que les bords)
- `grow-food` augmente la quantité de nourriture d'un patch et ajuste sa couleur
- `build-turtles` crée 100 tortues et les initialise (`init-turtle`) avec un stock de nourriture identique
- `colorier-tortue` ajuste la couleur des tortues en fonction du stock de nourriture qu'elles possèdent (vert : tout va bien ; rouge : la tortue a faim)
- `faim?` prévient la tortue qu'il faut manger (pour l'instant, cela ne dépend que de la valeur du stock de nourriture)
- `decide` est la procédure de décision (sélection d'action) d'une tortue. Si elle a faim elle cherche de la nourriture (chercher-nourriture), sinon elle erre dans l'environnement (errer). Chaque appel à decide coûte une unité de nourriture. Si la tortue n'en n'a plus, elle meurt.
- `errer` permet aux tortues de se déplacer aléatoirement dans l'environnement
- `chercher-nourriture` cherche le meilleur endroit (de préférence le patch où se trouve la tortue ou un de ses voisins) où manger (manger-ici) : pour manger, la tortue prend d'un coup toute la nourriture présente sur le patch. S'il ne contient pas suffisamment de nourriture, elle se déplace sur une case voisine


> **À noter :** Dans la simulation, le temps est subdivisé en unités appelées « pas de temps » (`ticks` dans NetLogo). Chaque tortue exécute exactement une fois decide à chaque tick.

## Sauvons les tortues !

1. **Pourquoi les tortues meurent-elles si rapidement ?**
2. **Ajoutez sur le graphique des agents une courbe rouge donnant le nombre de tortues affamées**
3. **Ajoutez un slider dans l'interface pour régler une variable `seuil-faim` variant entre 1 et 100, et modifiez la fonction `faim?` en conséquence. Que se passe-t-il si on augmente cette variable ? Pourquoi ?**


> Solutions : [sugarscape1-faim.nlogo](sugarscape1-faim.nlogo) 


## Hystérésis
Dans ce premier exemple, la sensation de faim n'a pas d'inertie, elle est instantanée. Le même seuil de faim déclenche ou arrête le comportement d’alimentation et celui-ci n’est donc pas persistant. On souhaite introduire un effet mémoire avec deux seuils, en utilisant le principe de l'hystérésis (mécanisme utilisé pour le thermostat) :

- si la tortue commence à manger, elle doit maintenir ce comportement jusqu'à un seuil haut (seuil de satiété) pour maximiser son stock ;
- si la tortue est occupée à autre chose, elle peut laisser son stock de nourriture descendre jusqu'à un seuil bas (seuil de faim).

On introduit donc une variable booléenne `eating?` dans chaque tortue pour savoir si la tortue est en train de manger. Cette variable est positionnée à `false` au début de la simulation et dès que la tortue n'a plus faim ; au contraire, elle doit être fixée à `true` dès que la tortue commence à avoir faim et doit chercher de la nourriture. 

On modifie également `faim?` comme suit : *Soit je ne suis pas en train de manger et le stock est inférieur à seuil-faim, soit je suis en train de manger et le stock est inférieur à seuil-satiete.*

Faites les modifications correspondantes, puis testez-les au moyen de deux sliders dans l'interface graphique (un pour chaque seuil). Lorsque les deux seuils sont égaux, on doit retrouver le même comportement que précédemment.

4. **Existe-t-il une configuration qui permette aux tortues de survivre indéfiniment ?  Pourquoi ?**

> Solutions : [sugarscape2-persistance.nlogo](sugarscape2-persistance.nlogo)

## Une mémoire géographique

La modification précédente a amélioré les choses, mais pas suffisamment. On souhaite maintenant donner aux tortues une mémoire pour conserver le patch le plus récent contenant de la nourriture, et ainsi y retourner directement sans avoir à chercher.

On introduit donc pour chaque tortue une variable `last-patch` (initialisée à `nobody`) mise à jour chaque fois que la tortue mange vraiment. Attention à la procédure `decide` : la tortue doit se diriger (cf. instruction `face`) vers `last-patch` (si celui-ci n'est pas `nobody`) lorsqu'elle ne sait pas où aller, autrement dit lorsqu'aucun patch voisin ne contient plus d'une unité de nourriture. Si elle ne connaît pas de patch contenant de la nourriture et qu'il n'y en a pas sur les patchs voisins, elle est évidemment réduite à errer.

Procédez aux modifications nécessaires puis testez les comportements, en commençant avec un seuil de faim égal à 20.

5. **Existe-t-il des configurations de paramètres pour lesquelles les tortues survivent ? Pourquoi ?**
6. **Comment pouvez-vous vérifier expérimentalement vos hypothèses ?**
7. **Comment expliquer qu'à certains moments les tortues se regroupent sur une des deux collines alors qu'elles ne communiquent pas ?**
8. **Peut-on se passer du mécanisme d'hystérésis mis en place précédemment ?**
9. **Quelle est la plus grosse population stable que vous obtenez ?**

> Solutions : [sugarscape3-memoire.nlogo](sugarscape3-memoire.nlogo)

## De plus en plus de tortues

Maintenant que l'on sait faire survivre une population, nous allons étudier plus finement la démographie des agents en leur permettant de se reproduire. 

Pour simplifier, nous considérons dans la suite qu'une tortue se reproduit sur place et seule (utilisez la procédure `hatch`) selon la règle suivante : 
*Si je n'ai pas faim et que j'ai un stock de nourriture ≥ 2 $\times$ `cout-reproduction`, je me reproduis.*
La reproduction coûte à la tortue `cout-reproduction` du stock de nourriture, mais elle donne à son descendant un stock de nourriture de même valeur. 
(Il peut donc y avoir plusieurs agents au même endroit.)

10. **Écrivez une fonction `can-reproduce?` qui teste si le stock de nourriture est suffisant, une procédure `reproduce` qui implémente la reproduction, puis modifiez `decide` en conséquence.**
11. **Vérifiez qu'avec un coût de reproduction suffisamment élevé (> 50) on retrouve bien le comportement précédent.**
12. **Diminuez le coût de la reproduction pour qu'il soit juste inférieur à la moitié du seuil de satiété. Qu'observez-vous ? Le résultat est-il qualitativement différent avec un coût de reproduction très faible (10 ou 20) ?**
13. **Est-il indispensable de disposer d'une mémoire pour observer ces résultats ?**
    Pour répondre, ajoutez un switch `memory?` dans l'interface pour indiquer si l'on met à jour l'attribut `last-patch`.
14. **Expliquez ce que l'on observe avec les paramètres suivants :** `seuil-faim` = 30, `seuil-satiete` = 100, `cout-reproduction` = 70, `memory?` = `true`

> Solutions : [sugarscape4-repro.nlogo](sugarscape4-repro.nlogo)

## Des chasseurs-cueilleurs aux agriculteurs
On permet maintenant aux tortues de semer des graines pour faire pousser plus de nourriture. 
La règle est la suivante : *si une tortue n'a pas faim, elle peut utiliser une partie de son stock (`dispo` = `stock` — `seuil-satiete` si cette quantité est supérieure à `seuil-semis`) pour ensemencer son environnement, 
c’est-à-dire mettre l'attribut `food-max` du patch local, s’il est à zéro, à la valeur `dispo`. 

On souhaite disposer d'une variable globale `nb-cultures` incrémentée chaque fois qu'un semis est effectué.

17. **Écrivez une procédure `cultiver` qui réalise les semis. Modifiez `decide` pour que `cultiver` soit effectué dès que possible. Ajoutez le nombre de patchs mis en culture dans les graphiques.**
18. **Dans quelles conditions (i.e. pour quels jeux de paramètres) les cultures apparaissent-elles ?**

> Solutions : [sugarscape5-semis.nlogo](sugarscape5-semis.nlogo)

# Références 
[^1]: Epstein JM, Axtell R. *Growing Artificial Societies. Social Science from the Bottom Up.* The Brookings Institution. 1996. http://sugarscape.sourceforge.net/

[^2]: Wilensky U, Rand W. *An introduction to agent-based modeling: modeling natural, social, and engineered complex systems with NetLogo.* Cambridge, Massachusetts: The MIT Press; 2015.

